//
//  AccountsUC.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/4/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import SimpleTools

protocol AccountsUC {
    var accounts: ArrayProvider<Account> { get }
    func observeAccount(idx: Int) -> Observable<Account?>

    var groupedAccounts: RowsProvider<Account> { get }
    func observeAccount(path: IndexPath) -> Observable<Account?>
}

class AccountsUCImpl: AccountsUC {
    private let accountsRepo: AccountsRepository = Resolver.shared.accountsRepository()
    private let networkRequest: NetworkRequest = Resolver.shared.networkRequest()
    private let globalState: GlobalState = Resolver.shared.globalState()
    
    var accounts: ArrayProvider<Account> {
        return self.accountsRepo.allItems
    }
    func observeAccount(idx: Int) -> Observable<Account?> {
        let account = self.accounts.arr[idx]
        let observable = self.accountsRepo.keyedItems.observe(key: account.id)
        return observable
    }
    
    var groupedAccounts: RowsProvider<Account> {
        return self.accountsRepo.allItemsGroupedByName
    }
    func observeAccount(path: IndexPath) -> Observable<Account?> {
        let account = self.groupedAccounts.arr[path]
        let observable = self.accountsRepo.keyedItems.observe(key: account.id)
        return observable
    }
    
    init() {
        if let user = self.globalState.user {
            self.networkRequest.accounts(user: user) { (response) in
                switch response {
                case .success(let accounts):
                    self.accountsRepo.save(accounts)
                default:
                    break
                }
            }
        }
        spendingLoop()
    }
    
    func spendingLoop() {
        Utils.afterDelay(seconds: 0.5) { [weak self] in
            if let spendingAccount = self?.accountsRepo.keyedItems.arr[safe: Constants.Debug.spendingAccountId] {
                spendingAccount.money -= 1
                self?.accountsRepo.save([spendingAccount])
            }
            self?.spendingLoop()
        }
    }
}
