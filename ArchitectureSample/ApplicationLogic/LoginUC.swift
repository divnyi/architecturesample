//
//  LoginUC.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/9/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

enum LoginDataInvalidType {
    case tooLong
    case tooShort
    case characterType
}
enum LoginDataInvalid {
    case username(type: LoginDataInvalidType)
    case password(type: LoginDataInvalidType)
}
enum LoginReponse {
    case networkFailure
    case wrongCredentials
    case loggedIn(user: User)
}

protocol LoginUC {
    func validate(username: String, password: String) -> LoginDataInvalid?
    func login(username: String, password: String, callback: @escaping ((LoginReponse) -> Void))
}

class LoginUCImpl: LoginUC {
    private let networkRequest: NetworkRequest = Resolver.shared.networkRequest()
    private var globalState: GlobalState = Resolver.shared.globalState()
    
    func validate(username: String, password: String) -> LoginDataInvalid? {
        if let incorrectType = checkString(str: username) {
            return .username(type: incorrectType)
        }
        if let incorrectType = checkString(str: password) {
            return .password(type: incorrectType)
        }
        return nil
    }
    
    func login(username: String, password: String, callback: @escaping ((LoginReponse) -> Void)) {
        assert(validate(username: username, password: password) == nil)
        self.networkRequest.login(username: username, password: password) { (resp) in
            switch resp {
            case .networkFailure:
                callback(.networkFailure)
            case .failure:
                callback(.wrongCredentials)
            case .success(let user):
                self.globalState.user = user
                callback(.loggedIn(user: user))
            }
        }
    }
    
    private func checkString(str: String) -> LoginDataInvalidType? {
        if (str.range(of: "^[a-zA-Z0-9]*$", options: .regularExpression, range: nil, locale: nil)) == nil {
            return .characterType
        } else if str.count < 6 {
            return .tooShort
        } else if str.count > 20 {
            return .tooLong
        } else {
            return nil
        }
    }
}
