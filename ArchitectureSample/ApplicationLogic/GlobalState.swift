//
//  GlobalState.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/10/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

protocol GlobalState {
    static var shared: GlobalState { get }
    
    var user: User? { get set }
}

class GlobalStateImpl: GlobalState {
    static let shared: GlobalState = GlobalStateImpl()
    
    var user: User?
}
