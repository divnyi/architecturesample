//
//  Utils.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/8/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class Utils: NSObject {
    public class func afterDelay(seconds: Double, fn: @escaping (() -> Void)) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            fn()
        }
    }
    
    public class func createVC<Type>(storyboardId: String, vcId: String) -> Type {
        let type = Type.self
        // swiftlint:disable:next force_cast
        let bundle = Bundle(for: type as! AnyClass)
        let storyboard = UIStoryboard.init(name: storyboardId, bundle: bundle)
        let vc = storyboard.instantiateViewController(withIdentifier: vcId)
        
        // swiftlint:disable:next force_cast
        return vc as! Type
    }
}
