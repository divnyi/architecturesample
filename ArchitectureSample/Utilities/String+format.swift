//
//  String+format.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/10/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

extension String {
    func formatArgs(_ objs: Any...) -> String {
        let stringObjs = objs.map { (obj) -> CVarArg in
            return "\(obj)"
        }
        return String(format: self, arguments: stringObjs)
    }
}
