//
//  AccountsVC.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/4/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import SimpleTools

class AccountsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, AccountsRouter {
    // MARK: - Constructor
    class func createVC() -> AccountsVC {
        let vc: AccountsVC = Utils.createVC(storyboardId: "Accounts", vcId: "AccountsVC")
        vc.title = Constants.Titles.accounts
        return vc
    }
    
    // MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView!
    
    // MARK: - variables
    private let viewModel: AccountsVM = Resolver.shared.accountsVM()
    private var disposal: Disposal = []
    
    private func subscribe() {
        self.disposal += self.viewModel.accounts.fullUpdate.observe { _ in
            self.tableView.reloadData()
        }
        self.disposal += self.viewModel.accounts.partialUpdate.observe { _ in
            // can write some logic to update only chosen rows...
            self.tableView.reloadData()
        }
        self.disposal += self.viewModel.toAccountDetails.observe({ (account) in
            self.showAccountDetails(account: account)
        })
    }
    
    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.subscribe()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.disposal.removeAll()
    }
    
    // MARK: - navigation
    func showAccountDetails(account: Observable<Account?>) {
        let accountDetailsVC = Resolver.shared.accountDetailsVM(account).createVC()
        self.navigationController?.pushViewController(accountDetailsVC, animated: true)
    }
 
    // MARK: - table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.accounts.arr.count()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let accountVM = self.viewModel.accounts.arr[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AccountCell", for: indexPath) as? AccountCell {
            cell.setup(accountVM: accountVM)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.selectedCell(idx: indexPath.row)
    }
}
