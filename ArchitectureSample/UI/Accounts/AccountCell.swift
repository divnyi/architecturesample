//
//  AccountCell.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/8/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class AccountCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    
    func setup(accountVM: AccountVM) {
        self.title.text = accountVM.title
        self.subtitle.text = accountVM.subtitle
    }
}
