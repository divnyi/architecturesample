//
//  AccountsVC.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/4/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import SimpleTools

class GroupedAccountsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, AccountsRouter {
    // MARK: - Constructor
    class func createVC() -> GroupedAccountsVC {
        let vc: GroupedAccountsVC = Utils.createVC(storyboardId: "Accounts", vcId: "GroupedAccountsVC")
        vc.title = Constants.Titles.groupedAccounts
        return vc
    }
    
    // MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView!
    
    // MARK: - variables
    private let viewModel: GroupedAccountsVM = Resolver.shared.groupedAccountsVM()
    private var disposal: Disposal = []
    
    private func subscribe() {
        self.disposal += self.viewModel.groupedAccounts.fullUpdate.observe { _ in
            self.tableView.reloadData()
        }
        self.disposal += self.viewModel.groupedAccounts.partialUpdate.observe { _ in
            // can write some logic to update only chosen rows...
            self.tableView.reloadData()
        }
        self.disposal += self.viewModel.toAccountDetails.observe({ (account) in
            self.showAccountDetails(account: account)
        })
    }
    
    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.subscribe()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.disposal.removeAll()
    }
    
    // MARK: - navigation
    func showAccountDetails(account: Observable<Account?>) {
        let accountDetailsVC = Resolver.shared.accountDetailsVM(account).createVC()
        self.navigationController?.pushViewController(accountDetailsVC, animated: true)
    }
 
    // MARK: - table view
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.groupedAccounts.arr.count.sections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.groupedAccounts.arr.count.rows(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let accountVM = self.viewModel.groupedAccounts.arr[indexPath]
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AccountCell", for: indexPath) as? AccountCell {
            cell.setup(accountVM: accountVM)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.selectedCell(path: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionName = self.viewModel.groupNames.arr[section]
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0))
        label.text = sectionName
        label.backgroundColor = Constants.Colors.GroupedAccounts.sectionView
        return label
    }
}
