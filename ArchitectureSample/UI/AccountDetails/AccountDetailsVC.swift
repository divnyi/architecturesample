//
//  AccountDetails.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/10/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import SimpleTools

class AccountDetailsVC: UIViewController {
    // MARK: - Constructor
    class func createVC(viewModel: AccountDetailsVM) -> AccountDetailsVC {
        let vc: AccountDetailsVC = Utils.createVC(storyboardId: "AccountDetails", vcId: "AccountDetailsVC")
        vc.viewModel = viewModel
        return vc
    }
    
    // MARK: - Outlets
    @IBOutlet weak var label: UILabel!
    
    // MARK: - variables
    private var viewModel: AccountDetailsVM!
    private var disposal: Disposal = []
    
    private func subscribe() {
        self.disposal += self.viewModel.niceString.observe({ (string) in
            self.label.text = string
        })
    }
    
    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Constants.Titles.accountDetails
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.subscribe()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.disposal.removeAll()
    }
}

extension AccountDetailsVM {
    /// convinience initializer
    func createVC() -> AccountDetailsVC {
        return AccountDetailsVC.createVC(viewModel: self)
    }
}
