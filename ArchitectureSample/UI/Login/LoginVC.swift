//
//  LoginVC.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/9/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import SimpleTools

class LoginVC: UIViewController, UITextFieldDelegate {
    // MARK: - Constructor
    class func createVC() -> LoginVC {
        return Utils.createVC(storyboardId: "Login", vcId: "LoginVC")
    }
    
    // MARK: - Outlets
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var warningLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - variables
    private let viewModel: LoginVM = Resolver.shared.loginVM()
    private let rootRouter: RootRouter = Resolver.shared.rootRouter()
    private var disposal: Disposal = []
    
    private func subscribe() {
        self.disposal += self.viewModel.usernameInput.observe({ (input) -> Void in
            switch input.highlight {
            case .none:
                self.usernameTF.backgroundColor = Constants.Colors.Login.textFieldNormal
            case .red:
                self.usernameTF.backgroundColor = Constants.Colors.Login.textFieldRed
            }
        })
        self.disposal += self.viewModel.passwordInput.observe({ (input) -> Void in
            switch input.highlight {
            case .none:
                self.passwordTF.backgroundColor = Constants.Colors.Login.textFieldNormal
            case .red:
                self.passwordTF.backgroundColor = Constants.Colors.Login.textFieldRed
            }
        })
        self.disposal += self.viewModel.warningText.observe({ (string) in
            self.warningLabel.text = string
        })
        self.disposal += self.viewModel.loading.observe({ (isSpinning) in
            self.activityIndicator.isHidden = !isSpinning
        })
        self.disposal += ComposedObserve.observe(
            self.viewModel.loading,
            self.viewModel.validated
        ) { loading, validated in
            let shouldShow = !loading && validated
            self.loginButton.isEnabled = shouldShow
            self.loginButton.backgroundColor = shouldShow ?
                Constants.Colors.Login.loginButtonActive :
                Constants.Colors.Login.loginButtonInactive            
        }
        self.disposal += self.viewModel.toAccountsNavigation.observe {
            self.rootRouter.showAccountsScreen()
        }
    }
    
    // MARK: - lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.subscribe()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.disposal.removeAll()
    }
    
    // MARK: - actions
    @IBAction func loginButtonClicked(_ sender: Any) {
        self.viewModel.performLogin(username: self.usernameTF.text ?? "", password: self.passwordTF.text ?? "")
    }
    
    // MARK: - text field
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        Utils.afterDelay(seconds: 0.05) {
            self.viewModel.validate(username: self.usernameTF.text ?? "", password: self.passwordTF.text ?? "")
        }
        return true
    }
}
