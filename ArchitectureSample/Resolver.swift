//
//  Resolver.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/8/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import SimpleTools

class Resolver: NSObject {
    // MARK: lambdas
    var rootRouter: (() -> RootRouter)!
    
    var loginVM: (() -> LoginVM)!
    var accountsVM: (() -> AccountsVM)!
    var groupedAccountsVM: (() -> GroupedAccountsVM)!
    var accountDetailsVM: ((Observable<Account?>) -> AccountDetailsVM)!

    var networkRequest: (() -> NetworkRequest)!
    var accountsRepository: (() -> AccountsRepository)!

    var globalState: (() -> GlobalState)!

    var loginUC: (() -> LoginUC)!
    var accountsUC: (() -> AccountsUC)!

    // MARK: -
    static let shared = Resolver()
    
    override init() {
        super.init()
        self.setup()
    }
    
    func setup() {
        self.rootRouter = { return RootRouterImpl.shared }
        
        self.loginVM = { return LoginVMImpl() }
        self.accountsVM = { return AccountsVMImpl() }
        self.groupedAccountsVM = { return GroupedAccountsVMImpl() }
        self.accountDetailsVM = { account in return AccountDetailsVMImpl(account: account) }

        self.networkRequest = { return NetworkRequestImpl() }
        self.accountsRepository = { return AccountsRepositoryImpl() }
        
        self.loginUC = { return LoginUCImpl() }
        self.accountsUC = { return AccountsUCImpl() }
        
        self.globalState = { return GlobalStateImpl.shared }
    }
    
    /// Don't use unless you really need it.
    ///
    /// Can be used to create several objects linked to same
    /// resolved value. You should reuse old resolvers anyway,
    /// so they can be mocked later
    static func withChangedResolver(_ fn: (Resolver) -> Void) {
        objc_sync_enter(Resolver.shared)
        defer { objc_sync_exit(Resolver.shared) }

        fn(Resolver.shared)
        Resolver.shared.setup()
    }
}
