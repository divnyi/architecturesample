//
//  NetworkRequest.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/9/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

enum NetworkResponse<Failure, Success> {
    case networkFailure
    case failure(_ response: Failure)
    case success(_ response: Success)
}

typealias NetworkResponseCallback<F, S> = ((NetworkResponse<F, S>) -> Void)

protocol NetworkRequest {
    func login(username: String,
               password: String,
               callback: @escaping NetworkResponseCallback<NetworkError.Login, User>)
    func accounts(user: User, callback: @escaping NetworkResponseCallback<Void, [Account]>)
}

class NetworkRequestImpl: NetworkRequest {
    func login(username: String,
               password: String,
               callback: @escaping NetworkResponseCallback<NetworkError.Login, User>) {
        Utils.afterDelay(seconds: Constants.Debug.fakeNetworkResponseTime) {
            if username == Constants.Debug.username && password == Constants.Debug.password {
                let user = Constants.Debug.fakeUser
                callback(.success(user))
            } else {
                callback(.failure(NetworkError.Login.wrongCredentials))
            }
        }
    }
    func accounts(user: User, callback: @escaping NetworkResponseCallback<Void, [Account]>) {
        Utils.afterDelay(seconds: Constants.Debug.fakeNetworkResponseTime) {
            callback(.success(Constants.Debug.accountChanges))
        }
    }
}
