//
//  LoginVM.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/9/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import SimpleTools

protocol LoginVM {
    var usernameInput: Observable<InputVM> { get }
    var passwordInput: Observable<InputVM> { get }
    var warningText: Observable<String?> { get }
    var loading: Observable<Bool> { get }
    var validated: Observable<Bool> { get }
    
    var toAccountsNavigation: EventStream<Void> { get }
    
    func validate(username: String, password: String)
    func performLogin(username: String, password: String)
}

class LoginVMImpl: LoginVM {
    let useCase: LoginUC = Resolver.shared.loginUC()
    
    let usernameInput = Observable(InputVM(highlight: .none))
    let passwordInput = Observable(InputVM(highlight: .none))
    let warningText = Observable<String?>(nil)
    let loading: Observable<Bool> = Observable<Bool>(false)
    let validated: Observable<Bool> = Observable<Bool>(false)
    var toAccountsNavigation = EventStream<Void>()
    
    func validate(username: String, password: String) {
        self.clean()
        
        if let validationError = self.useCase.validate(username: username, password: password) {
            self.validated.value = false
            let warnings = Constants.Texts.Login.Warning.self
            switch validationError {
            case .username(let type):
                self.usernameInput.value = InputVM(highlight: .red)
                self.warningText.value = warnings.usernameIncorrect.formatArgs(type.toString())

            case .password(let type):
                self.passwordInput.value = InputVM(highlight: .red)
                self.warningText.value = warnings.passwordIncorrect.formatArgs(type.toString())
            }
        } else {
            self.validated.value = true
        }
    }
    
    func performLogin(username: String, password: String) {
        self.clean()
        self.loading.value = true

        self.useCase.login(username: username, password: password) { (response) in
            self.loading.value = false

            let warnings = Constants.Texts.Login.Warning.self
            switch response {
            case .networkFailure:
                self.warningText.value = warnings.networkFailure
            case .wrongCredentials:
                self.warningText.value = warnings.wrongCredentials.formatArgs(Constants.Debug.username,
                                                                              Constants.Debug.password)
            case .loggedIn:
                self.toAccountsNavigation.send(obj: ())
            }
        }
    }
    
    private func clean() {
        self.usernameInput.value = InputVM(highlight: .none)
        self.passwordInput.value = InputVM(highlight: .none)
        self.warningText.value = nil
    }
}

extension LoginDataInvalidType {
    func toString() -> String {
        let warningAppendix = Constants.Texts.Login.Warning.Appendix.self
        switch self {
        case .tooShort:
            return warningAppendix.tooShort
        case .tooLong:
            return warningAppendix.tooLong
        case .characterType:
            return warningAppendix.invalidType
        }
    }
}
