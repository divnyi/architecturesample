//
//  InputVM.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/9/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

enum InputHighlight {
    case none
    case red
}

struct InputVM {
    let highlight: InputHighlight
}
