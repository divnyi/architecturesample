//
//  AccountDetails.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/10/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import SimpleTools

protocol AccountDetailsVM {
    init(account: Observable<Account?>)

    var niceString: Observable<String> { get }
}

class AccountDetailsVMImpl: AccountDetailsVM {
    // MARK: - input
    private let account: Observable<Account?>

    // MARK: - output
    let niceString = Observable("")

    // MARK: - internal
    private var disposal: Disposal = []

    required init(account: Observable<Account?>) {
        self.account = account
        self.disposal += self.account.observe { [weak self] (account) in
            if let account = account {
                self?.niceString.value = Constants.Texts.AccountDetails.niceString
                    .formatArgs(account.name, account.money)
            } else {
                self?.niceString.value = Constants.Texts.AccountDetails.accountDeleted
            }
        }
    }
}
