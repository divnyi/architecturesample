import UIKit
import SimpleTools

protocol GroupedAccountsVM {
    var groupedAccounts: RowsProvider<AccountVM> { get }
    var groupNames: ArrayProvider<String> { get }

    var toAccountDetails: EventStream<Observable<Account?>> { get }
    
    func selectedCell(path: IndexPath)
}

class GroupedAccountsVMImpl: GroupedAccountsVM {
    private let useCase: AccountsUC = Resolver.shared.accountsUC()
    
    let groupedAccounts: RowsProvider<AccountVM>
    let groupNames: ArrayProvider<String>
    let toAccountDetails = EventStream<Observable<Account?>>()
    
    init() {
        self.groupedAccounts = useCase.groupedAccounts.transform({ (acc: Account) -> AccountVM in
            return AccountVM(account: acc)
        })
        self.groupNames = ArrayProvider<String>(
            arr: LazyArray1d(
                count: { () -> Int in
                    // nobody counts titles anyway
                    return 0
            },
                item: { (idx) -> String in
                    let startingValue = Int(("A" as UnicodeScalar).value)
                    let char = Character(UnicodeScalar(startingValue + idx)!)
                    return "\(char)"
            }),
            partialUpdate: EventStream<ArrayUpdate>(),
            fullUpdate: EventStream<Void>())
    }
    
    func selectedCell(path: IndexPath) {
        let observable = self.useCase.observeAccount(path: path)
        self.toAccountDetails.send(obj: observable)
    }
}
