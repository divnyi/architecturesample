import UIKit

class AccountVM: NSObject {
    var title: String
    var subtitle: String
    
    convenience init(account: Account) {
        self.init(title: account.name, subtitle: "\(account.money) moneyz")
    }
    
    init(title: String, subtitle: String) {
        self.title = title
        self.subtitle = subtitle
    }
}
