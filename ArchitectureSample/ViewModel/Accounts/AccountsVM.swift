import UIKit
import SimpleTools

protocol AccountsVM {
    var accounts: ArrayProvider<AccountVM> { get }
    
    var toAccountDetails: EventStream<Observable<Account?>> { get }
    
    func selectedCell(idx: Int)
}

class AccountsVMImpl: AccountsVM {
    private let useCase: AccountsUC = Resolver.shared.accountsUC()

    let accounts: ArrayProvider<AccountVM>
    let toAccountDetails = EventStream<Observable<Account?>>()
    
    init() {
        self.accounts = useCase.accounts.transform({ (acc: Account) -> AccountVM in
            return AccountVM(account: acc)
        })
    }
    
    func selectedCell(idx: Int) {
        let observable = self.useCase.observeAccount(idx: idx)
        self.toAccountDetails.send(obj: observable)
    }
}
