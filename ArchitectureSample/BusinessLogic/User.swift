//
//  User.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/9/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class User: NSObject {
    var id: Int
    var sessionId: String
    
    init(id: Int, sessionId: String) {
        self.id = id
        self.sessionId = sessionId
    }
}
