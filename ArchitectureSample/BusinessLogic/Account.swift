//
//  Account.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/4/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class Account: NSObject {
    var id: Int
    var name: String
    var money: Double
    
    init(id: Int, name: String, money: Double) {
        self.id = id
        self.name = name
        self.money = money
    }
}
