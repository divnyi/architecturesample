//
//  Constants.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/10/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

//swiftlint:disable nesting
class Constants: NSObject {
    class Debug: NSObject {
        static let username = "qwerty"
        static let password = "qwerty"
        
        static let spendingAccountId = 10
        static let initialAccounts = [
            Account(id: 0, name: "Secret Account", money: 12345345435.99),
            Account(id: 1, name: "Public Account", money: 100.01),
            Account(id: 2, name: "My cat's account", money: 404),
            Account(id: spendingAccountId, name: "Spending Account", money: 9999.0),
            ]
        static let accountChanges = [
            Account(id: 0, name: "Secret Account (frozen)", money: 0.0),
            Account(id: 1, name: "Public Account", money: 100.01),
            Account(id: 2, name: "My cat's account", money: 404),
            Account(id: 3, name: "Police account", money: 12345345435.99),
            ]
        
        static let fakeNetworkResponseTime = 2.0
        static let fakeUser = User(id: 123, sessionId: "abcdefg!")
    }
    
    class Colors: NSObject {
        class Login: NSObject {
            static let textFieldNormal = UIColor.white
            static let textFieldRed = UIColor(red: 1.0, green: 0.8, blue: 0.8, alpha: 1.0)
            static let loginButtonActive = UIColor(red: 0, green: 0.5, blue: 1.0, alpha: 1.0)
            static let loginButtonInactive = UIColor.gray
        }
        class GroupedAccounts: NSObject {
            static let sectionView = UIColor.lightGray
        }
    }
    
    class Titles: NSObject {
        static let accounts = "Accounts"
        static let groupedAccounts = "GroupedAccounts"
        static let accountDetails = "Details"
    }
    
    class Texts: NSObject {
        class Login: NSObject {
            class Warning: NSObject {
                static let usernameIncorrect = "Username invalid - %@."
                static let passwordIncorrect = "Password invalid - %@."
                static let networkFailure = "Network failure - please try again or call... nevermind, try again"
                static let wrongCredentials = """
Username or password is incorrect

We won't tell you real ones,\nbut they are totally not \"%@\" \"%@\"
"""
                
                class Appendix: NSObject {
                    static let tooShort = "too short"
                    static let tooLong = "too long"
                    static let invalidType = "character type invalid (only letters and numbers)"
                }
            }
        }
        class AccountDetails: NSObject {
            static let niceString = "Account name:\n%@\n\nMoneyz:\n%@"
            static let accountDeleted = "It seems like account was deleted"
        }
    }
}
