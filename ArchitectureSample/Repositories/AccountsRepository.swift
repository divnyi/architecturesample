//
//  AccountsRepository.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/4/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import SimpleTools

protocol AccountsRepository {
    func save(_ items: [Account])
    func delete(_ items: [Account])
    
    var keyedItems: KeyedProvider<Int, Account> { get }
    var allItems: ArrayProvider<Account> { get }
    var allItemsGroupedByName: RowsProvider<Account> { get }
}

class AccountsRepositoryImpl: AccountsRepository {
    private var accountsMap: [Int: Account] = [:] // use this map instead of actual database
    
    private lazy var keyedProvider: KeyedProvider<Int, Account>! = KeyedProvider<Int, Account>(
        arr: KeyedLazyArray(
            count: { [weak self] () -> Int in
                return self?.accountsMap.count ?? 0
            },
            item: { [weak self] (key: Int) -> Account in
                if let item = self?.accountsMap[key] {
                    return item
                }
                throw LazyArrayErrors.outOfBounds
            }
        ),
        partialUpdate: EventStream<KeyedUpdate<Int>>(),
        fullUpdate: EventStream<Void>()
    )
    
    private lazy var provider: ArrayProvider<Account>! = ArrayProvider<Account>(
        arr: LazyArray(
            count: { [weak self] in
                return self?.accountsMap.count ?? 0
            }, item: { [weak self] (idx) -> Account in
                if let item = self?.accountsMap.map({ $0.value })[idx] {
                    return item
                }
                throw LazyArrayErrors.outOfBounds
            }
        ),
        partialUpdate: EventStream<ArrayUpdate>(),
        fullUpdate: EventStream<Void>()
    )
    
    private lazy var groupedProvider: RowsProvider<Account>! = RowsProvider<Account>(
        arr: LazyArray2d(count: ({ () -> Int in
            let startingValue = Int(("a" as UnicodeScalar).value)
            let lastValue = Int(("z" as UnicodeScalar).value)
            let count = lastValue - startingValue + 1
            return count
        }, { [weak self] (section) -> Int in
            if let items = self?.accountsMap.map({ $0.value }).filter({ (acc) -> Bool in
                let startingValue = Int(("a" as UnicodeScalar).value)
                let char = Character(UnicodeScalar(startingValue + section)!)
                return acc.name.lowercased().first == char
            }) {
                return items.count
            }
            return 0
        }), item: { [weak self] (path) -> Account in
            if let items = self?.accountsMap.map({ $0.value }).filter({ (acc) -> Bool in
                let startingValue = Int(("a" as UnicodeScalar).value)
                let char = Character(UnicodeScalar(startingValue + path.section)!)
                return acc.name.lowercased().first == char
            }) {
                let item = items[path.row]
                return item
            }
            throw LazyArrayErrors.outOfBounds
        }),
        partialUpdate: EventStream<RowsUpdate>(),
        fullUpdate: EventStream<Void>()
    )
    
    init() {
        // act as if we had something before...
        for acc in Constants.Debug.initialAccounts {
            self.accountsMap[acc.id] = acc
        }
    }
    
    private func sendFullUpdate() {
        self.provider.fullUpdate.send(obj: Void())
        self.groupedProvider.fullUpdate.send(obj: Void())
        self.keyedProvider.fullUpdate.send(obj: Void())
    }
    
    func save(_ items: [Account]) {
        for item in items {
            let id = item.id
            self.accountsMap[id] = item; // do the insertion
        }
        // that's not very easy thing to order stuff that comes from hashmaps..
        // thus I'm too lazy to setup partialUpdates properly
        self.sendFullUpdate()
    }
    
    func delete(_ items: [Account]) {
        for item in items {
            let id = item.id
            self.accountsMap[id] = nil; // do the deletion
        }
        self.sendFullUpdate()
    }
    
    var keyedItems: KeyedProvider<Int, Account> {
        return self.keyedProvider
    }
    
    var allItems: ArrayProvider<Account> {
        return self.provider
    }
    
    var allItemsGroupedByName: RowsProvider<Account> {
        return self.groupedProvider
    }
}
