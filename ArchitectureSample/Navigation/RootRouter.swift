//
//  RootRouter.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/10/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

protocol RootRouter {
    func showLoginScreen()
    func showAccountsScreen()
}

class RootRouterImpl: RootRouter {
    private var window: UIWindow
    
    static let shared = RootRouterImpl()
    
    init() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window.makeKeyAndVisible()
    }
    
    func showLoginScreen() {
        self.window.rootViewController = LoginVC.createVC()
    }
    
    func showAccountsScreen() {
        Resolver.withChangedResolver { resolver in
            let account = resolver.accountsUC()
            resolver.accountsUC = { return account }
            // use same account for both views
            let generalAccountList = AccountsVC.createVC()
            let groupedAccountList = GroupedAccountsVC.createVC()
            
            let tabbar = UITabBarController()
            tabbar.viewControllers = [
                UINavigationController(rootViewController: generalAccountList),
                UINavigationController(rootViewController: groupedAccountList),
            ]
            
            self.window.rootViewController = tabbar
        }
    }
}
