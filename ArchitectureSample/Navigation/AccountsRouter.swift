//
//  AccountsRouter.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/10/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import SimpleTools

protocol AccountsRouter {
    func showAccountDetails(account: Observable<Account?>)
}
